
FROM continuumio/miniconda3

COPY environment.yml /
RUN env
RUN conda env create -f environment.yml

RUN mkdir notebook
COPY run.sh /
COPY notebook/my_notebook.ipynb notebook/my_notebook.ipynb
COPY notebook/my_number.yml notebook/my_number.yml 

RUN apt-get update
RUN pip install papermill
RUN pip install ipykernel

#RUN conda init bash
RUN /bin/bash -c "activate docker-experiment"
RUN ipython kernel install --user --name=docker_experiment

CMD ["/bin/bash", "./run.sh"]
